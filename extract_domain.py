import requests
import json
import time
from urllib.parse import urlparse


def guarda_rdap():
    response = requests.get('https://data.iana.org/rdap/dns.json')
    step1 = json.loads(response.content.decode('utf-8'))
    return step1


def test_domain(domain, tld):

    data_rdap = {'status_code': [], 'error_reason': [], 'cnpj': [], 'dominio': [], 'data_registro': [], 'url_rdap': []}

    pos = 0
    try:
        for x in rdap['services']:
            if tld in rdap['services'][pos][0]:
                busca_tld = rdap['services'][pos][0]
                busca_server = rdap['services'][pos][1]

                break
            if pos == 823:
                data_rdap['status_code'].append('418')
                return data_rdap
            else:
                pos += 1

    except Exception as segundo_erro:
        data_rdap['status_code'].append('419')
        return data_rdap

    try:
        # noinspection PyUnboundLocalVariable
        response = requests.get(busca_server[0] + 'domain/' + domain)
        passo1 = json.loads(response.content.decode('utf-8'))
        data_rdap['status_code'].append(response.status_code)
        if str(response.status_code).startswith('4'):
            data_rdap['error_reason'].append(response.reason)
        data_rdap['dominio'].append(passo1['ldhName'])
        data_rdap['data_registro'].append(passo1['events'][0]['eventDate'])
        data_rdap['url_rdap'].append(busca_server[0])
        if len(passo1['entities'][0]['handle']) == 14:
            data_rdap['cnpj'].append(passo1['entities'][0]['handle'])
            return data_rdap
        else:
            return data_rdap
    except Exception as erro:
        data_rdap['status_code'].append('418')
        return data_rdap


def extract_domain(url):

    uri = urlparse(url)

    uri_scheme = ('file', 'ftp', 'gopher', 'hdl', 'https', 'http', 'imap', 'mailto', 'mms', 'news', 'nntp', 'prospero', 'rsync', 'rtsp', 'rtspu', 'sftp', 'shttp', 'sip', 'sips', 'snews', 'svn', 'svn+ssh', 'telnet', 'wais', 'ws', 'wss')

    if url.startswith('www.') and ':' not in url:
        passo1 = f"{uri.path}"

        if '/' in passo1:
            passo2 = passo1.split('/', 1)[0]
            passo3 = passo2.split('.', 1)[1]
            passo4 = test_domain(passo3, passo3.rsplit('.', 1)[1])
            if passo4['status_code'][0] == 200:
                return passo4
            else:
                return False

        if '/' not in passo1:
            passo2 = passo1.split('.', 1)[1]
            passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
            if passo3['status_code'][0] == 200:
                return passo3
            else:
                return False

    if url.startswith('www.') and ':' in url:
        passo1 = urlparse(url).scheme
        passo2 = passo1.split('.', 1)[1]
        passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
        if passo3['status_code'][0] == 200:
            return passo3
        else:
            return False

    if url.startswith('ww.'):
        passo1 = url.split('.', 1)[1]
        if ':' in passo1:
            passo2 = passo1.split(':')[0]
            passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
            if passo3['status_code'][0] == 200:
                return passo3
            else:
                return False

        if '/' in passo1:
            passo2 = passo1.split('/')[0]
            passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
            if passo3['status_code'][0] == 200:
                return passo3
            else:
                return False
        else:
            passo2 = test_domain(passo1, passo1.rsplit('.', 1)[1])
            if passo2['status_code'][0] == 200:
                return passo2
            else:
                return False

    elif url.startswith(uri_scheme):
        url = url.lower().strip()
        conta = 0
        for x in uri_scheme:
            passo1 = len(uri_scheme[conta])
            passo2 = uri_scheme[conta][0:passo1]
            if passo2 == url[0:passo1]:
                passo3 = url.strip(url[0:passo1])
                try:
                    while passo3.startswith('/'):
                        passo3 = passo3.replace('/', '', 1)
                except:
                    continue
                try:
                    while passo3.startswith(':'):
                        passo3 = passo3.replace(':', '', 1)
                except:
                    continue
                try:
                    while passo3.startswith('/'):
                        passo3 = passo3.replace('/', '', 1)
                except:
                    continue

                if '/' in passo3:
                    passo4 = passo3.split('/', 1)[0]
                    passo5 = test_domain(passo4, passo4.rsplit('.', 1)[1])
                    if passo5['status_code'][0] == 200 and passo5['dominio'][0].rsplit('.', 1)[1] == 'br':
                        passo6 = passo4.split('.', 1)[1]
                        passo7 = test_domain(passo6, passo6.rsplit('.', 1)[1])
                        if passo7['dominio'][0] == 'com.br':
                            return passo5
                        else:
                            return passo7
                    if passo5['status_code'][0] == 200:

                        return passo5
                    else:
                        passo6 = passo4.split('.', 1)[1]
                        passo7 = test_domain(passo6, passo6.rsplit('.', 1)[1])
                        if passo7['status_code'][0] == 200:
                            return passo7
                        else:
                            return False
                else:
                    passo4 = test_domain(passo3, passo3.rsplit('.', 1)[1])
                    if passo4['status_code'][0] == 200:
                        return passo4
                    else:
                        passo5 = passo3.split('.', 1)[1]
                        passo6 = test_domain(passo5, passo5.rsplit('.', 1)[1])
                        if passo6['status_code'][0] == 200:
                            return passo6
                        else:
                            return False
            else:
                conta += 1

    elif '/' in url:
        passo1 = url
        while passo1.startswith('/'):
            passo1 = passo1.replace('/', '', 1)

        if '/' in passo1:
            passo2 = passo1.split('/')[0]
            passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
            if passo3['status_code'][0] == 200 and passo3['dominio'][0].rsplit('.', 1)[1] == 'br':
                passo4 = passo2.split('.', 1)[1]
                passo5 = test_domain(passo4, passo4.rsplit('.', 1)[1])
                if passo5['dominio'][0] == 'com.br':
                    return passo3
                else:
                    return passo5
            if passo3['status_code'][0] == 200:
                return passo3
            if passo3['status_code'][0] != 200:
                try:
                    passo4 = passo2.split('.', 1)[1]
                    passo5 = test_domain(passo4, passo4.rsplit('.', 1)[1])
                    if passo5['status_code'][0] == 200:
                        return passo5
                    else:
                        return False
                except:
                    return False

        else:
            passo2 = test_domain(passo1, passo1.rsplit('.', 1)[1])
            if passo2['status_code'][0] == 200:
                return passo2
            if passo2 != 200:
                passo3 = passo1.split('.', 1)[1]
                passo4 = test_domain(passo3, passo3.rsplit('.', 1)[1])
                if passo4['status_code'][0] == 200:
                    return passo4
                else:
                    return False

    else:
        domain_name = f"{uri.path}"
        try:
            passo1 = test_domain(domain_name, domain_name.rsplit('.', 1)[1])
            if passo1['status_code'][0] == 200:
                return passo1
            else:
                passo2 = domain_name.split('.', 1)[1]
                passo3 = test_domain(passo2, passo2.rsplit('.', 1)[1])
                if passo3['status_code'][0] == 200:
                    return passo3
                else:
                    return False
        except:
            return False

start = time.time()

rdap = guarda_rdap()
# # # Entrada de dados
input_data = {'site': 'http://artecompimenta.com.br', 'mail': 'renato@artecompimenta.com.br'}

step0 = extract_domain(input_data['site'])
print(step0)
end = time.time()
print(f'tempo: {end-start:.2f}')
